# Contributing to Content

Expand or write your own scenario using the using the scene structure outline in this document. Then send a merge request to allow others to try your scenario. If no major bugs are found it a maintainer will merge it into master.

## Folder structure

```javascript
data {
  scenario {
    girls
    scenes
    quotes
  }
}
```

## Scenes

The scenes are stored in nested objects as anonymous functions. Functions and properties are stuctured as follows.

```javascript
go(location: string);
s(line: string);
c(label: string, option: string);
```
