# Melissa Explains It All

A Typescript rewrite of the original omorashi game created by **drw**. The engine and story scenes are seperated with the goal of having a more robust system for further content expansions.

# Contributing

Feel free to clone the repo and add your own scenarios. Merge requests are welcome.

Please see the [CONTRIBUTING.md](CONTRIBUTING.md) file for the required syntax for story scenes.

## Transpiling

[Rollup](https://rollupjs.org) is used to transpile and bundle all the scripts into one data file. You can start the development environment using:

> `npm run dev`

## Publish

All commits to the master branch are automatically published using GitLab Pages. You can find the latest version at https://hollyleaves.gitlab.io/melissa

# License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

# Acknowledgments

The original storyline and scenario contents were created by **drw**.
