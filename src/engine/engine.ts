import { writable } from "svelte/store";

import Game from "engine/game";
import Girl from "./girl";

export let desc = writable<string[]>([]);
export let choices = writable<[string, string][]>([]);
export let stats = writable<[string, string][]>([]);

export let game: Game;
export let girl: Girl;

export function startGame(scenario: string) {
  game = new Game(scenario);
  girl = new Girl();
}

export function go(loc: () => void) {
  desc.set([]);
  choices.set([]);
  loc();
}

export function s(line: string) {
  desc.update((val) => {
    return [...val, line];
  });
}

export function c(choice: string, label: string) {
  choices.update((val) => {
    return [...val, [choice, label]];
  });
}
