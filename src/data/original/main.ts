import type { Scenario } from "data/data";
import * as scenes from "data/original/scenes";

export const original: Scenario = {
  intro: scenes.intro,
  scenes: scenes,
};
