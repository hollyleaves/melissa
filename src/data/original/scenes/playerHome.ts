import { c, s, girl } from "engine/engine";

export const intro = () => {
  s("This game is a date simulation with a desperation twist.");
  s("Your goal is to get your date in bed with a full bladder, before the night is over.");
  s("On your way there, convince your date to hold her pee, pee in a glass, squat outside, or even take you into the restroom with her. Just don't make her wet her panties...");
  s("Attraction points are your key to success. They indicate how much she's into you, and therefore what she will do for you.");
  s("Shyness points control how shy she acts around you.");
  s("The bladder meter shows how much she's holding... and she has definite physical limits in this department.");
  s("What's in her tummy slowly flows to her bladder as time passes... with realistic modeling.");
  s("There are many useful objects you can get in the game. Don't blow all your cash in one place.");

  c("playerHome.start", "Start the game.");
};

export const playerHome = {
  start: () => {
    s("You have a hot date tonight at 8pm.");
    s(`Her name is ${girl.name}.`);
    s("You've heard she might be into desperation play... but you're not sure.");
    s("So what are you going to do?");

    c("store", "Go to the store");
    c("phone.callher", "Call her on the phone");
    c("girlHome.pickup", "Pick her up");
  },
  main: () => {
    s("You're looking forward to your date, and it's getting near time to pick her up.");
    s("So what are you going to do?");

    c("store", "Go to the store");
    c("phone.callher", "Call her on the phone");
    c("girlHome.pickup", "Pick her up");
  },
};
