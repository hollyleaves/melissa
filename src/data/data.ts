type Scene = () => void;

interface SceneGroup {
  [key: string]: Scene | SceneGroup;
}

export interface Scenario {
  intro: Scene;
  scenes: SceneGroup;
}

export interface Data {
  [key: string]: Scenario;
}

import { original } from "data/original/main";

const data: Data = {
  original: original,
};

export default data;
